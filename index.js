function getFloatNumberWithDot(value) {
    if (value && value.includes(',')) {
        return parseFloat(value.replace(',', '.'));
    } else {
        return parseFloat(value);
    }
}

function getFloatNumberWithComma(value) {
    if (value && value.includes('.')) {
        return value.replace('.', ',');
    } else {
        return value;
    }
}

function truncateNumberWithoutRounding(value, numberOfDecimals = 2) {
    const numberPartials = value.split('.');

    if (numberPartials.length > 1) {
        const decimals = numberPartials[1].substring(0, numberOfDecimals);

        return [numberPartials[0], decimals].join('.');
    } else {
        return value;
    }
}

module.exports.getFloatNumberWithDot = getFloatNumberWithDot;
module.exports.getFloatNumberWithComma = getFloatNumberWithComma;
module.exports.truncateNumberWithoutRounding = truncateNumberWithoutRounding;